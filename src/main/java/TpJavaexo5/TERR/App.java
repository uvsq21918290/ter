package TpJavaexo5.finalTer;

import java.util.PriorityQueue;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App {



    public static void main( String[] args )
    {
        System.out.println( "\n =========================Simulation=========================== \n" );
   
        System.out.println( " order inter-class : \n" );
        
        Priority pq=new Priority();
        MessageData m1 = new MessageData("pack_d1", 3.2); 
        pq.ajout_data(m1); 
        MessageData m2 = new MessageData("pack_d2", 5.6); 
        pq.ajout_data(m2);          
        MessageData m3 = new MessageData("pack_d3", 4.0); 
        pq.ajout_data(m3);
      
      
        
        MessageVoix mv1 = new MessageVoix("pack_v1", 7.2); 
        pq.ajout_voix(mv1); 
        MessageVoix mv2 = new MessageVoix("pack_v2",8.1 ); 
        pq.ajout_voix(mv2);          
        MessageVoix mv3 = new MessageVoix("pack_v3", 2.4); 
        pq.ajout_voix(mv3);
        
        
        pq.affiche_data();
        pq.affiche_voix();
        System.out.println("\n order intra-class: \n");
        pq.compareto();
      
    } 
       
}
    

