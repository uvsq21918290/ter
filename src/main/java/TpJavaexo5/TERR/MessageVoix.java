package TpJavaexo5.TERR;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;


public class MessageVoix{
	
	private String voix;
	 double priority ;
	
	 public MessageVoix(String voix, double priority) { 
		  
		    this.voix = voix; 
		    this.priority = priority; 
		} 
		
		public  double getpriority() {
			return priority;
		}
		
		public String getVoix() {
			return voix;
		}
		
}
