package TpJavaexo5.TERR;

import java.util.Comparator;
import java.util.PriorityQueue;
	
class Message { 
	
	public MessageData data; 
	public  MessageVoix voice; 
	public Message test;
	public double priority;
    
	public Message() {}
	public Message(MessageData data, MessageVoix voice) { 
	  
	    this.data = data; 
	    this.voice = voice; 
	} 
	  
	public MessageData get_Data() { 
	    return data; 
	}  
	public  MessageVoix get_Voice() { 
	    return voice; 
	}  
	
	public  Message test() {
		return new Message(data,voice);
		
	}
	
	
}
